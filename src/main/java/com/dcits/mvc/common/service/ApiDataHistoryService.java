package com.dcits.mvc.common.service;

import java.util.List;

import com.dcits.mvc.common.model.ApiDataHistory;

public class ApiDataHistoryService {
	private static final ApiDataHistory dao = new ApiDataHistory().dao();
	
	public void save(ApiDataHistory data) {
		data.save();
	}
	
	public boolean del(Integer id) {
		return dao.deleteById(id);
	}
	
	public ApiDataHistory get(Integer id) {
		return dao.findById(id);
	}
	
	/**
	 * 根据需求标识查询
	 * @return
	 */
	public ApiDataHistory findByIdentity(String identity) {
		return dao.findFirst("select * from " + ApiDataHistory.TABLE_NAME + " where " + ApiDataHistory.column_api_identity
					+ "=?", identity);
	}
	
	public List<ApiDataHistory> listAll() {
		return dao.find("select * from " + ApiDataHistory.TABLE_NAME);
	}
} 
