package com.dcits.mvc.common.service;

import com.dcits.mvc.common.model.CommandConfig;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.tool.StringUtils;

import javax.rmi.CORBA.Util;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * description: 获取指令配置
 *
 * @author sun jun
 * @className CommandConfigService
 * @date 2020/8/28 16:05
 */
public class CommandConfigService {

    public static final CommandConfig dao = new CommandConfig().dao();

    public boolean edit(CommandConfig commandConfig) {
        if (commandConfig.getId() == null) {
            commandConfig.setCreateTime(new Date());
            return commandConfig.save();
        } else {
            return commandConfig.update();
        }
    }

    public List<CommandConfig> listAll(String briefCommand, int configId, String useFlag) {
        String sql = "select * from " + CommandConfig.TABLE_NAME + " where " + CommandConfig.column_config_id + "=?";
        if (StringUtils.isNotEmpty(useFlag)) {
            sql += " and useFlag='" + useFlag + "'";
        }
        if (StringUtils.isNotEmpty(briefCommand)) {
            sql += " and briefCommand='" + briefCommand + "'";
        }
        return dao.find(sql, configId);
    }

    public List<CommandConfig> listByPage(String briefCommand, int configId, String useFlag, int limit, int page) {
        String sql = "select * from " + CommandConfig.TABLE_NAME + " where " + CommandConfig.column_config_id + "=?";

        if (StringUtils.isNotEmpty(briefCommand)) {
            sql += " and briefCommand='" + briefCommand + "'";
        }
        if (StringUtils.isNotEmpty(useFlag)) {
            sql += " and useFlag='" + useFlag + "'";
        }

        sql += " limit " + (page - 1) * limit + "," + page * limit + " ";
        System.out.println(sql);

        return dao.find(sql, configId);
    }

    public List<CommandConfig> findCommandByBriefCommand(String briefCommand, String useFlag, int configId) {
        String sql = "select * from " + CommandConfig.TABLE_NAME + " where " + CommandConfig.column_config_id + "=?";

        if (StringUtils.isNotEmpty(briefCommand)) {
            sql += " and briefCommand= '" + briefCommand + "'";
        }

        if (StringUtils.isNotEmpty(useFlag)) {
            sql += " and useFlag='" + useFlag + "'";
        }
        System.out.println(sql);
        return dao.find(sql, configId);
    }

    public void deleteById(int id) {
        dao.deleteById(id);
    }

    public void batchDelete(String ids) {
        for (String id : ids.split(",")) {
            deleteById(Integer.parseInt(id));
        }
    }

}


