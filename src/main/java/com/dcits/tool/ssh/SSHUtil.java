package com.dcits.tool.ssh;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import com.dcits.business.server.linux.LinuxServer;
import com.dcits.business.server.websocker.WebSocketController;
import com.dcits.dto.ResponseSocketDTO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import org.apache.log4j.Logger;

import javax.websocket.EncodeException;
import java.io.*;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class SSHUtil {

    private static final Logger logger = Logger.getLogger(SSHUtil.class);

    private static final Set<String> EXEC_COMMAND_TAGS = new HashSet<String>();

    /**
     * 获取ssh连接
     *
     * @param host
     * @param port
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    public static Connection getConnection(String host, int port, String username, String password) throws IOException {
        Connection conn = new Connection(host, port);

        try {
            conn.connect(null, 3000, 2500);
            boolean flag = conn.authenticateWithPassword(username, password);
            if (flag) {
                return conn;
            }
        } catch (IOException e) {

            logger.error(host + ":" + port + "连接失败!", e);
            throw e;
        }
        return null;

    }

    /**
     * 通过jsch获取连接
     *
     * @param host
     * @param port
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    public static com.jcraft.jsch.Session getJschSession(String host, int port, String username, String password) throws JSchException {
        JSch jsch = new JSch();
        com.jcraft.jsch.Session session = null;

        try {
            session = jsch.getSession(username, host);
            session.setPassword(password);

            Properties props = new Properties();
            props.put("StrictHostKeyChecking", "no");
            session.setConfig(props);
            session.connect();
        } catch (JSchException e) {
            logger.error("开启session失败:\n" + e.getMessage());
            throw e;
        }
        return session;
    }


    /**
     * 发送停止标记
     *
     * @param tag
     */
    public static void stopExecCommand(String tag) {
        EXEC_COMMAND_TAGS.add(tag);
    }

    /**
     * 执行一次命令
     * <br>返回结果
     * <br>命令一定要是一次性执行完毕的命令(非交互性命令)
     *
     * @param conn    ssh连接对象
     * @param command 执行命令
     * @param count   读取多少行,从第一行开始读取
     * @param getMode 返回模式，0-只返回正确的输出 1-只返回错误的信息 2-返回所有的信息<br>mode=2时将会使用终端模式
     * @param tag     标记,防止中断命令时在并发情况下出现混乱
     * @return
     * @throws Exception
     * @throws IOException
     */
    public static String execCommand(Connection conn, String command, int count, int getMode, String tag) throws Exception {
        StringBuilder str = new StringBuilder();

        if (conn != null) {
            Session session = null;
            InputStream is = null;
            BufferedReader brStat = null;
            try {
                session = conn.openSession();

                if (getMode == 1) {
                    is = new StreamGobbler(session.getStderr());
                }

                if (getMode != 1) {
                    is = new StreamGobbler(session.getStdout());
                    if (getMode == 2) {
                        session.requestPTY("vt100", 80, 24, 640, 480, null);
                    }
                }

                session.execCommand(command);
                brStat = new BufferedReader(new InputStreamReader(is));

                String readLine = null;
                int i = 0;
                while (i < count && (readLine = brStat.readLine()) != null) {

                    str.append(readLine);

                    i++;

                    if (i != count) {
                        str.append("\n");
                    }
                    //判断是否有中断命令
                    if (EXEC_COMMAND_TAGS.contains(tag)) {
                        break;
                    }
                }
            } catch (Exception e) {
                throw e;
            } finally {

                closeStream(brStat, is, session);
            }
        }

        return str.toString();
    }


    /**
     * description: 执行linuxzhi零
     *
     * @param conn          ssh的connnect
     * @param command       指令
     * @param socketSession websocket的session
     * @param socketDTO     websocket 返回对象
     * @param tag           无限流的中止标识
     * @return void 无返回值，直接读取行返回
     * @author sunjun
     * @date 2020/9/24 8:48
     */
    public static void execCommandFlow(Connection conn, String command, javax.websocket.Session socketSession, ResponseSocketDTO socketDTO, String tag) throws IOException, EncodeException {


        if (conn != null) {
            Session session = null;
            InputStream is = null;
            BufferedReader brStat = null;
            try {
                session = conn.openSession();

                is = new StreamGobbler(session.getStdout());

                session.requestPTY("vt100", 80, 24, 640, 480, null);

                session.execCommand(command);
                brStat = new BufferedReader(new InputStreamReader(is));

                String readLine = null;
                int i = 0;
                while ((readLine = brStat.readLine()) != null) {
                    socketDTO.setCommandResponse(readLine.toString());
                    System.out.println(readLine.toString());
//                    socketSession.getBasicRemote().sendObject(socketDTO);

                    WebSocketController.PutMessageToQueue(socketSession, socketDTO, tag);
                    if (WebSocketController.FLAG) {
                        WebSocketController.sendQueueMsg();
                        WebSocketController.FLAG = false;
                    }
                    if (EXEC_COMMAND_TAGS.contains(tag)) {
                        break;
                    }
                }
            } catch (Exception e) {
                throw e;
            } finally {
                closeStream(brStat, is, session);
            }
        }
    }

    private static void closeStream(BufferedReader brStat, InputStream is, Session session) {
        if (brStat != null) {
            try {
                brStat.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }

        if (session != null) {
            session.close();
        }
    }

    /**
     * 持续命令的执行
     * <br>返回输出流
     *
     * @param conn
     * @param command
     * @return
     * @throws IOException
     */
    public static SSHBufferedReader execLoopCommand(Connection conn, String command) throws IOException {
        Session session = conn.openSession();

        InputStream is = new StreamGobbler(session.getStdout());
        SSHBufferedReader brStat = new SSHBufferedReader(new InputStreamReader(is), session);
        session.execCommand(command);
        return brStat;
    }

    public static boolean channelExec(LinuxServer server, String command, javax.websocket.Session socketSession, ResponseSocketDTO socketDTO, String tag) throws JSchException {
        Channel channel = null;
        try {
            channel = server.getJschSession().openChannel("shell");
        } catch (JSchException e) {
            server.getJschSession().disconnect();
            server.connectssh();
            channel = server.getJschSession().openChannel("shell");
        }

        //通道连接 超时时间3s
        channel.connect(3000);

//            //设置channel
        server.setChannel(channel);

        //转发消息
        InputStream inputStream = null;
        try {
            transToSSH(channel, command);
            inputStream = channel.getInputStream();
            //循环读取
            byte[] buffer = new byte[1024];
            int i = 0;
            //如果没有数据来，线程会一直阻塞在这个地方等待数据。
            while ((i = inputStream.read(buffer)) != -1) {
                String readLine = new String(buffer, 0, i);
                logger.debug(readLine);
                socketDTO.setCommandResponse(readLine);
                WebSocketController.PutMessageToQueue(socketSession, socketDTO, tag);
                if (WebSocketController.FLAG) {
                    WebSocketController.sendQueueMsg();
                    WebSocketController.FLAG = false;
                }
                if (EXEC_COMMAND_TAGS.contains(tag)) {
                    channel.disconnect();
                    server.setChannel(null);
                    break;
                }

            }
            return true;

        }catch (Exception e){
            channel.disconnect();
            server.setChannel(null);
            logger.error(e);
        }  finally {
            //断开连接后关闭会话
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        }

        return false;

    }

    /**
     * @Description: 将消息转发到终端
     * @Param: [channel, data]
     * @return: void
     * @Author: NoCortY
     * @Date: 2020/3/7
     */
    public static void transToSSH(Channel channel, String command) throws IOException {
        if (channel != null) {
            OutputStream outputStream = channel.getOutputStream();
            outputStream.write(command.getBytes());
            outputStream.flush();
        }
    }


}
