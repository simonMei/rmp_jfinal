var wpoint = ""
function WSSHClient(point) {
    wpoint = point
};

WSSHClient.prototype._generateEndpoint = function () {
    // if (window.location.protocol == 'https:') {
    //     var protocol = 'wss://';
    // } else {
    //     var protocol = 'ws://';
    // }
    // var herf =  location.href.substr(0,location.href.indexOf('/html')).replace(location.protocol,'ws:')+'/websocket';
    //
    // var endpoint = "ws://localhost:8080/rmp_war_exploded/websocket";
    return wpoint;
 };

WSSHClient.prototype.connect = function (options) {
    var endpoint = this._generateEndpoint();
    if (window.WebSocket) {
        //如果支持websocket
        this._connection = new WebSocket(endpoint);
        console.log("如果支持websocket")
    }else {
        //否则报错
        options.onError('WebSocket Not Supported');
        return;
    }

    this._connection.onopen = function () {
        options.onConnect();
    };

    this._connection.onmessage = function (evt) {
        var data = evt.data.toString();
        //data = base64.decode(data);
        options.onData(data);
    };


    this._connection.onclose = function (evt) {
        options.onClose();
    };
};

WSSHClient.prototype.closeSocket = function () {
    this._connection.onclose = function () {
        console.log('socket连接关闭');
    }
};

WSSHClient.prototype.send = function (data) {
    this._connection.send(JSON.stringify(data));
};

WSSHClient.prototype.sendInitData = function (options) {
    //连接参数
    this._connection.send(JSON.stringify(options));
}

WSSHClient.prototype.sendClientData = function (data) {
    //发送指令
    this._connection.send(JSON.stringify(data))
}

var client = new WSSHClient();
